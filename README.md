# My Cinema Bot

## Использование

Имя бота в телеграме `cinebo_bot`.

1. Чтобы получить справку, введите `/help` или `/start`.
2. Чтобы отправить запрос введите строку в таком формате `[<запрос>]` 
(без угловых скобочек). Например, `[Venom]`.
3. Далее в формате `@<число>` требуется выбрать номер искомого фильма из предложенного списка.
3. На сообщения в других форматах бот не реагирует.

## Как работает

- Бот размещен на бесплатном сервере от `heroku`. 
- Использует [pyTelegramBotAPI](https://github.com/eternnoir/pyTelegramBotAPI) для взаимодействия с API телеграма.
- Для поиска информации используется база `TMDb`, API которой декорирует библиотека [tmdbsimple](https://github.com/celiao/tmdbsimple).
- Ссылки для просмотра дает как сформированный поисковый запрос на соответствующий ресурс.
